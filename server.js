//ustawienia i główne procesy aplikacji

const mongoose = require('mongoose') //dostęp do bazy i operacja bazodanowe
require('dotenv').config() //obsługa pliku konfiguracyjnego  .env

const express = require('express') //wczytanie biblioteki
const app = express(); // utworzenie obiektu frameworka - do późniejszej konfiguracji

// konfiguracja express
app.use( express.json())
//router
const carsRouter = require('./routes/cars')
app.use('/cars', carsRouter) //'jakby' RESTowy zasób

//obsługa bazy danych
mongoose.connect( process.env.MONGO_DB_URL, {useNewUrlParser:true, useUnifiedTopology: true})
const dbConnection = mongoose.connection

dbConnection.on('error', (error) => console.error(error)) //reakcja na dowolny błąd bazy
dbConnection.once('open', () => console.log('Połączono z bazą danych'))

app.listen(5555, () => console.log('Uruchomiono serwer'))