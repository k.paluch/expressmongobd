//komponent pośredniczący w obsłudze żadania
const config = require('config')
const jwt = require('jsonwebtoken')

module.exports = function (req,res, next) {
    const token = req.headers['x-access-token'] || req['authorization']
    if (!token) { //sprawdzenie powinno być szersze niż tylko obecność
        return res.status(401).send('Access denied, no valid token provided')
    }

    try {
        const decoded = jwt.verify(token, config.get('privatekey'))
        req.user = decoded;
        next(); //jeśli nie wystąpił błąd to przekierowujemy żadanie dalej
    } catch (error) {
        res.status(400).send('Invalid token')
    }

}