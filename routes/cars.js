const auth = require('../middleware/auth')
const express = require('express')
const router = express.Router()
const Car = require('../models/car') //model Car udostępdnia operacje bazodanowe

router.get('/', auth, async (req, res) => { //auth powoduje sprawdzenie serwera
    try {
        const cars = await Car.find() //find wyszukuje elementy w kolekcji (tabeli) wg podanych kryteriów lub wszystkie jęlsi brak kryteriów
        //console.log(cars)
        res.json(cars) //zwracamy wynik jako JSON
    } catch (error) {
        res.status(500).json({message: error.message})
    }
})


router.post('/', async (req, res) => {
    const car = new Car ({ //utworzenie rekordu 'car' w celu zapisania do bazy
        make: req.body.make,
        model: req.body.model,
        buildYear: req.body.buildYear,
        color: req.body.color

    })
    try {
        const newCar = await car.save() // wykonuje się asynchronicznie ZAWSZE ! dlatego 0 - jeśli chcemy mieć wynik to MUSIMY na niego zaczekać - await !
        const token = car.generateAuthToken() //w oparciu o _id zapisanego obiektu
        res.header('x-access-token', token)
        res.status(201).json(newCar)
    } catch (error) {
        res.status(400).json({message: error.message}) // HTTP 400 - bad request
    }
})

async function getCar(req, res, next) {
    try {
        car = await Car.findById(req.params.id)
        if (null == car) {
            return res.status(404).json({message: "Can't find the car with givern id"})
        }
    } catch (error) {
        return  res.status(500).json({message: error.message})
    }
    res.car = car
    next();
}

router.get("/:id", getCar, (req,res) => {
    res.json(res.car)
})

router.delete('/:id', getCar, async (req, res) => {
    try {
        await res.car.remove()
        res.json({message: "Car has been removed"})
    } catch (error) {
        return res.status(500).json({message: error.message})
    }
})

router.patch('/:id', getCar, async (req,res) => {
    if(req.body.make != null) {
        res.car.make = req.body.make
    }
    if(req.body.model != null) {
        res.car.model = req.body.model
    }
    if(req.body.buildYear != null) {
        res.car.buildYear = req.body.buildYear
    }
    if(req.body.color != null) {
        res.car.color = req.body.color
    }
    try {
        const  updatedCar = await car.save()
        res.json(updatedCar)
    } catch (error) {
        return res.status(400).json({message: error.message})
    }
})

module.exports = router

