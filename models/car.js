const mongoose = require('mongoose')
const config = require('config')
const jwt = require('jsonwebtoken')

//definicja schematu wg którego będą zapisywane i odczywtywane dane z bazy
const carSchema = new mongoose.Schema({
    make: {
        type: String,
        required: true,
        minlength: 3
    },
    model: {
        type: String,
        required: true,
        minlength: 2,
        maxlength: 15
    },
    buildYear: {
        type: Number,
        required: true,
        min: 2005
    },
    color: {
        type: String,
        required: false
    }

})



carSchema.methods.generateAuthToken = function () {
    const token = jwt.sign({_id: this._id}, config.get('privatekey')); //typowo zawiera login, uprawnienia, czas ważnosci
    return token
}


module.exports = mongoose.model('Car', carSchema)